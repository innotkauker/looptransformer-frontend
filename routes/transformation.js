var express = require('express');
var router = express.Router();
var spawn = require('child_process').spawn;
var fs = require('fs');
var uuid = require('node-uuid');
var auth = require('./auth');

var translator = (express().get('env') === 'development')
                  ? '/media/d0/repo/rose-transformations/LoopTransformer/translator'
                  : '/media/d0/repo/rose-transformations-stable/LoopTransformer/translator';
var working_dir = (express().get('env') === 'development')
                  ? '/tmp/LoopTransformer-dev/<uuid>'
                  : '/tmp/LoopTransformer/<uuid>';
var maxtime = 10800000; // 3 hours
var guest_maxtime = 10000; // 10 seconds

function toJSON(obj) {
  return JSON.stringify(obj) + "\n";
}

// Given a list of filenames:
// /tmp/nonsense-0/address/file0
// /tmp/nonsense-1/address/file1
// ...
// Move them into *wd* and delete original directories.
function aggregate_files(files, wd) {
  return files.map(function(f, i) {
    if (i === 0)
      return f;
    var t = f.match(/(.*)(\/[^\/]*)$/);
    var new_name = wd + t[2];
    fs.rename(f, new_name, function (err) { if (err !== null) console.log(err); });
    var base_dir = t[1].match(/(.*)\/[^\/]*/)[1];
    fs.rmdir(t[1], function (err) { if (err !== null) console.log(err); });
    fs.rmdir(base_dir, function (err) { if (err !== null) console.log(err); });
    return new_name;
  });
}

// Replaces < and > with corresponding HTML escape sequences.
function escapeHTML(data) {
  return data.replace(/[<>]/g, function(m) {
    if (m === '>')
      return '&gt;';
    else
      return '&lt;';
  });
}

router.post('/transform-file', function(req, res, next) {
  var files = req.files['target_file'];
  if (!Array.isArray(files))
    files = [files];
  if ((files === undefined) || !files.length || (files[0].filename === '')) {
    res.end(toJSON({ error: 'No file provided.' }));
    return;
  }
  var cwd = working_dir.replace('<uuid>', String(uuid.v4()));
  fs.mkdir(cwd, function(err) {
    if (err !== null)
      res.end(toJSON({ error: 'Internal error (could not create a working dir). Retry.' }));
  });

  var filenames = aggregate_files(files.map(function(f) { return f.file; }), cwd);
  var tr = spawn(translator, filenames, {
                  stdio: ['ignore', 'pipe', 'pipe'],
                  cwd: cwd,
  });
  var timeout = (req.cookies.passphrase === auth.passphrase) ? maxtime : guest_maxtime;
  var timer = setTimeout(function() {
    tr.kill('SIGHUP');
  }, timeout);

  req.on('close', function() {
    // \todo cleanup?
    console.log('Client disconnected inexpectedly');
    tr.kill('SIGHUP');
  });

  tr.on('error', function (err) {
    clearTimeout(timer);
    res.end(toJSON({ error: 'Internal error. Retry.' }));
    if (err.code === 'ENOENT')
      fs.mkdir(cwd, function(err) { console.log(err); }); // No working dir?
  });

  tr.stdout.on('data', function (data) {
    res.write(toJSON({ log: escapeHTML(String(data)) }));
  });

  tr.stderr.on('data', function (data) {
    res.write(toJSON({ log: escapeHTML(String(data)) }));
  });

  tr.on('close', function (code, signal) {
    clearTimeout(timer);
    if (code === 0 || code === 23) {
      if (files.length === 1) {
        var f = files[0];
        var tr_output = cwd + '/rose_' + f.filename;
        fs.readFile(tr_output, function(err, data) {
          if (err)
            res.end(toJSON({ error: err }));
          res.write(toJSON({ result: String(data) }));
          res.end(toJSON({ ready: 'file' }));
          // remove the files
          fs.unlink(tr_output, function(err) {});
          fs.rmdir(cwd, function(err) {});
          fs.unlink(f.file, function(err) {});
        });
      } else {
        fnames = files.map(function(f) { return cwd + '/rose_' + f.filename; });
        var zip = spawn('zip', ['-'] + fnames);
        zip.stdout.on('data', function(data) {
          res.write(toJSON({ result: data }));
        });
        zip.on('exit', function(code) {
          if (code)
            res.end(toJSON({ error: "Couldn't create an archive" }));
          res.end(toJSON({ ready: 'archive' }));
        });
      }
    } else {
      if (code === null)
        switch (signal) {
          case 'SIGHUP':
            res.end(toJSON({
              error: 'Execution timed out. Current limit is '
                                  + String(timeout) + ' milliseconds.',
            }));
            break;
          default:
            res.end(toJSON({
              error: 'The translator was killed. Please file a bug report.',
            }));
        }
      else
        res.end(toJSON({ error: 'Translator returned ' + String(code) }));
    }
  });

});

module.exports = router;
