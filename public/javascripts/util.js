String.prototype.capitalizeFirstLetter = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}

var createAlert = function(type, message, header) {
  if (typeof header === undefined)
    header = type.capitalizeFirstLetter() + ':';
  var err = document.createElement('div');
  var alert_type = 'alert-' + type;
  var dismiss_btn = '<button type="button" class="close" data-dismiss="alert" '+
                    'aria-label="Close"><span aria-hidden="true">&times;'+
                    '</span></button>'
  err.className = 'alert ' + alert_type + ' alert-fixed-top alert-dismissible';
  err.innerHTML = dismiss_btn + '<strong>' + header + '</strong> ' + message;
  document.body.appendChild(err);
};

var responseToJSON = function() {
  var buffer = '';
  return function(data) {
    buffer += data;
    var x = buffer.split("\n");
    buffer = x[x.length - 1];
    return x.slice(0, -1).map(function(e) {
      return JSON.parse(e);
    });
  };
}();

function classifyMessage(m) {
  var tr_log = m.match(/^\[([A-Z]*)\]: (.*)/);
  var spray_log = m.match(/^([A-Z]*): (.*)/);
  if (tr_log) {
    var lvl;
    switch (tr_log[1]) {
      case 'DEBUG': lvl = ''; break;
      case 'INFO': lvl = 'info'; break;
      case 'WARNING': lvl = 'warning'; break;
      case 'ERROR': lvl = 'danger'; break;
      default: lvl = undefined;
    };
    if (lvl === undefined)
      return { type: undefined, lvl: '', lvl_msg: '', message: m };
    return { type: 'translator', lvl: lvl, lvl_msg: tr_log[1], message: tr_log[2] };
  } else if (spray_log) {
    return { type: 'spray', lvl: '', lvl_msg: spray_log[1], message: spray_log[2] };
  } else {
    return { type: undefined, lvl: '', lvl_msg: '', message: m };
  }
};

function logMessage(m) {
  if (m === "")
    return;
  var log_item = document.createElement('li');
  var cl = classifyMessage(m);
  var item_lvl = (cl.lvl === '' ? '' : ' list-group-item-' + cl.lvl); // set color
  var type_cl = ' ' + (cl.type === undefined ? 'other-msg' : cl.type + '-msg'); // set type
  var lvl_cl = (cl.type !== 'translator'
                ? ''
                : ' translator-msg-' + (cl.lvl
                                        ? cl.lvl
                                        : 'debug')); // set translator message type
  log_item.className = 'list-group-item' + item_lvl + type_cl + lvl_cl;
  log_item.innerHTML = (cl.lvl_msg === '' ?
                        cl.message :
                        '<strong>' + cl.lvl_msg + ':</strong> ' + cl.message);
  var container = $('#log-container');
  var scroll = (container.attr('height') - 100 < container.scrollTop());
  container.append(log_item);
  if (scroll)
    container.scrollTop(container.attr('height'));
}

var handleResult = function () {
  var result = '';
  return function(data, done) {
    if (done) {
      $('#result-btn').click(function() {
        if (result === '')
          return;
        var f = data === 'file';
        this.href = "data:" + (f ? "text/plain;charset=UTF-8"
                                 : "application/zip")
                            + "," + encodeURIComponent(result);
        this.download = (f ? 'result.c' : 'result.zip');
        result = '';
      });
      $('#post-controls').removeClass('hidden');
    } else {
      result += data;
    }
  };
}();

var handleResponse = function() {
  var buffer = "";
  return function(data) {
    if (data.error !== undefined) {
      createAlert('danger', String(data.error), 'Transformation error:');
      buffer = '';
    } else if (data.log !== undefined) {
      $('#transformation-results').removeClass('hidden');
      $('#cover-heading').addClass('hidden');
      buffer += data.log;
      var messages = buffer.split("\n");
      // if a part of the message is left, store it in the buffer
      buffer = (messages.slice(-1)[0] !== "") ? messages.slice(-1)[0] : "";
      messages.slice(0, -1).forEach(logMessage);
    } else if (data.result !== undefined) {
      handleResult(data.result);
    } else if (data.ready !== undefined) {
      buffer = '';
      handleResult(data.ready, true);
    }
  };
}();

function clearResults() {
  // clear log
  var container = document.getElementById('log-container');
  while (container.firstChild)
    container.removeChild(container.firstChild);
  // hide controls
  $('#post-controls').addClass('hidden');
};

function logEntriesVisibility(cb) {
  var name = '.' + cb.attr('id').slice(0, -3); // remove '-cb'
  if (cb.is(':checked'))
    jss.remove(name);
  else
    jss.set(name, { 'display': 'none'});
  if (name === '.translator-msg') // toggle all translator controls
    toggleTranslatorControls(cb.is(':checked'));
};

function toggleTranslatorControls(on) {
  $('#log-filters').find('input[id^="translator-msg-"]').prop('disabled', !on);
  $('#translator-msg-cb').prop('disabled', false);
};

function resetView() {
  $('#post-controls').addClass('hidden');
  $('#transformation-results').addClass('hidden');
  $('#cover-heading').removeClass('hidden');
  clearResults();
};

